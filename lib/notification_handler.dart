import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:silicone/pages/auth/user/chat_page.dart';
import 'package:silicone/pages/wrapper.dart';
import 'package:silicone/providers/auth_provider.dart';
import 'package:silicone/providers/user_provider.dart';

class NotificationHandler extends StatefulWidget {
  final String payload;
  NotificationHandler({this.payload});
  @override
  _NotificationHandlerState createState() => _NotificationHandlerState();
}

class _NotificationHandlerState extends State<NotificationHandler> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final AuthProvider auth = Provider.of<AuthProvider>(context);
    if (!auth.isBusy && !auth.loggedIn) return Wrapper();
    final UserProvider userProvider = Provider.of<UserProvider>(context);
    if (widget.payload != null) {
      var payloadObj = jsonDecode(widget.payload);
      print(widget.payload);
      if (payloadObj['type'] != null) {
        if (payloadObj['type'] == 1) {
          if (payloadObj['idFrom'] != null && userProvider.users.isNotEmpty) {
            return ChatPage(
              user: userProvider.getUserById(payloadObj['idFrom']),
              currentUser: auth.user,
              isNotification: true,
            );
          }
        }
      }
    }
    return Scaffold(
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
