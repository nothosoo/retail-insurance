import 'package:flutter/material.dart';

class LoadingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: Center(
          child: Container(
            width: MediaQuery.of(context).size.width * 0.5,
            child: Image(
              image: AssetImage('images/logo.png'),
            ),
          ),
        ),
      ),
    );
  }
}
