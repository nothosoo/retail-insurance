import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:silicone/pages/auth/program_page.dart';
import 'package:silicone/pages/auth/user/dashboard.dart';
import 'package:silicone/pages/loading_page.dart';
import 'package:silicone/pages/login_page.dart';
import 'package:silicone/providers/auth_provider.dart';
import 'package:silicone/providers/program_provider.dart';

class Wrapper extends StatefulWidget {
  final bool isNotification;

  const Wrapper({Key key, this.isNotification = false}) : super(key: key);
  @override
  _WrapperState createState() => _WrapperState();
}

class _WrapperState extends State<Wrapper> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    AuthProvider authProvider = Provider.of<AuthProvider>(context);
    Widget showWidget;
    if (widget.isNotification) {
      ProgramProvider programProvider = Provider.of<ProgramProvider>(context);
      if (authProvider.isBusy || programProvider.isBusy)
        showWidget = LoadingPage();
      else if (authProvider.loggedIn != null && !authProvider.loggedIn)
        showWidget = LoginPage();
      else if (authProvider.loggedIn != null &&
          authProvider.loggedIn &&
          programProvider.programs.isNotEmpty &&
          programProvider.activeProgram == null)
        showWidget = ProgramPage();
      else if (programProvider.activeProgram != null) showWidget = Dashboard();
    } else {
      if (authProvider.isBusy)
        showWidget = LoadingPage();
      else if (authProvider.loggedIn != null && authProvider.loggedIn)
        showWidget = ProgramPage();
      else
        showWidget = LoginPage();
    }

    return AnimatedSwitcher(
      duration: Duration(milliseconds: 500),
      child: showWidget,
    );
  }
}
