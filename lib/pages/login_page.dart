import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:provider/provider.dart';
import 'package:silicone/components/button.dart';
import 'package:silicone/providers/auth_provider.dart';
import 'package:silicone/utils/constant.dart';

const String USER_NAME_KEY = 'username';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController _usernameController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  FlutterSecureStorage _storage = FlutterSecureStorage();
  bool remember = false;
  String hash = '';
  bool isValid = false;
  bool isBusy = false;
  @override
  void initState() {
    _usernameController.text = '';
    _passwordController.text = '';
    _readUsername();
    super.initState();
  }

  _readUsername() async {
    var username = await _storage.read(key: USER_NAME_KEY);
    if (username != null) {
      setState(() {
        _usernameController.text = username;
        remember = true;
      });
    }
  }

  validate(str) {
    if (_usernameController.text != '' && _passwordController.text != '') {
      setState(() {
        isValid = true;
      });
    } else {
      setState(() {
        isValid = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final AuthProvider authProvider = Provider.of<AuthProvider>(context);
    final double width = MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;
    final node = FocusScope.of(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('images/bg.jpeg'),
            fit: BoxFit.cover,
          ),
        ),
        child: Container(
          height: height,
          padding: const EdgeInsets.all(40),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image(
                image: AssetImage('images/logo.png'),
                width: width * 0.5,
              ),
              SizedBox(height: 40),
              // Container(
              //   width: width,
              //   child: Header2(text: 'Нэвтрэх', align: TextAlign.left),
              // ),
              // SizedBox(height: 20),
              TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Нэвтрэх нэр',
                    contentPadding: const EdgeInsets.all(0),
                  ),
                  controller: _usernameController,
                  onChanged: validate,
                  textInputAction: TextInputAction.next,
                  onEditingComplete: () => node.nextFocus()),
              SizedBox(height: 20),
              TextFormField(
                decoration: InputDecoration(
                  labelText: 'Нууц үг',
                  contentPadding: const EdgeInsets.all(0),
                ),
                controller: _passwordController,
                obscureText: true,
                onChanged: validate,
                onFieldSubmitted: (str) async {
                  if (isValid && !isBusy) {
                    setState(() {
                      isBusy = true;
                    });
                    var res = await authProvider.login(
                      _usernameController.text,
                      _passwordController.text,
                    );
                    if (res == null)
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          backgroundColor: PRIMARY_COLOR,
                          content: Text('Таны оруулсан мэдээлэл буруу байна'),
                        ),
                      );
                    if (remember) {
                      _storage.write(
                        key: USER_NAME_KEY,
                        value: _usernameController.text,
                      );
                    } else {
                      _storage.delete(key: USER_NAME_KEY);
                    }
                    setState(() {
                      isBusy = false;
                    });
                  } else
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        backgroundColor: PRIMARY_COLOR,
                        content: Text('Талбаруудыг бөглөнө үү?'),
                      ),
                    );
                },
              ),
              SizedBox(height: 20),
              Row(
                children: [
                  Checkbox(
                    fillColor: MaterialStateProperty.all(PRIMARY_COLOR),
                    value: remember,
                    onChanged: (val) {
                      setState(() {
                        remember = val;
                      });
                    },
                  ),
                  GestureDetector(
                    child: Text('Нэвтрэх нэр сануулах'),
                    onTap: () {
                      setState(() {
                        remember = !remember;
                      });
                    },
                  ),
                ],
              ),
              SizedBox(height: 20),
              SizedBox(
                height: 50,
                child: SizedBox.expand(
                  child: PrimaryButton(
                    color: PRIMARY_COLOR,
                    text: 'Нэвтрэх',
                    loading: isBusy,
                    onPressed: isValid && !isBusy
                        ? () async {
                            setState(() {
                              isBusy = true;
                            });
                            var res = await authProvider.login(
                              _usernameController.text,
                              _passwordController.text,
                            );
                            if (res == null)
                              ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                  backgroundColor: PRIMARY_COLOR,
                                  content: Text(
                                      'Таны оруулсан мэдээлэл буруу байна'),
                                ),
                              );
                            if (remember) {
                              _storage.write(
                                key: USER_NAME_KEY,
                                value: _usernameController.text,
                              );
                            } else {
                              _storage.delete(key: USER_NAME_KEY);
                            }
                            setState(() {
                              isBusy = false;
                            });
                          }
                        : () {
                            ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(
                                backgroundColor: PRIMARY_COLOR,
                                content: Text('Талбаруудыг бөглөнө үү?'),
                              ),
                            );
                          },
                  ),
                ),
              ),
              Text(hash),
            ],
          ),
        ),
      ),
    );
  }
}
