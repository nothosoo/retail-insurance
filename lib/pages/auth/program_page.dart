import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:silicone/pages/auth/program_list_page.dart';
import 'package:silicone/pages/auth/user/dashboard.dart';
import 'package:silicone/providers/program_provider.dart';

class ProgramPage extends StatefulWidget {
  @override
  _ProgramPageState createState() => _ProgramPageState();
}

class _ProgramPageState extends State<ProgramPage> {
  @override
  Widget build(BuildContext context) {
    ProgramProvider programProvider = Provider.of<ProgramProvider>(context);

    return Container(
      color: Colors.white,
      child: programProvider.isBusy
          ? Center(
              child: CircularProgressIndicator(),
            )
          : programProvider.activeProgram == null
              ? ProgramListPage()
              : Dashboard(),
    );
  }
}
