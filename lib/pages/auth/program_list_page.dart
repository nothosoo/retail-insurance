import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:silicone/components/button.dart';
import 'package:silicone/flat_icon_icons.dart';
import 'package:silicone/providers/program_provider.dart';
import 'package:silicone/utils/constant.dart';
import 'package:shimmer/shimmer.dart';

class ProgramListPage extends StatelessWidget {
  Color getProgramColor(int id) {
    if (id == 1)
      return PRIMARY_COLOR;
    else if (id == 2)
      return SECONDARY_COLOR;
    else if (id == 3)
      return SUCCESS_COLOR;
    else
      return GRAY_COLOR;
  }

  Widget buildProgramIcon(int id) {
    return Icon(
      id == 1
          ? FlatIcon.flaticonanalytics
          : id == 2
              ? FlatIcon.flaticontechnology
              : id == 3
                  ? FlatIcon.flaticonusers
                  : FlatIcon.flaticoncalendar,
      color: getProgramColor(id),
    );
  }

  @override
  Widget build(BuildContext context) {
    ProgramProvider programProvider =
        Provider.of<ProgramProvider>(context, listen: false);
    return Scaffold(
      body: SafeArea(
        child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('images/bg.jpeg'),
                fit: BoxFit.cover,
              ),
            ),
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.all(25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: 60),
                Container(
                  width: MediaQuery.of(context).size.width * 0.5,
                  child: Image(image: AssetImage('images/logo.png')),
                ),
                SizedBox(height: 30),
                // Header2(text: 'Retail Insurance 3.0', color: TEXT_COLOR),
                // Text('Даатгалын удирдлагын мэдээллийн систем'),
                if (programProvider.programs.isNotEmpty)
                  Expanded(
                    child: GridView.builder(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        childAspectRatio: 5 / 6,
                      ),
                      shrinkWrap: true,
                      itemCount: programProvider.programs.length,
                      itemBuilder: (context, index) => Container(
                        padding: const EdgeInsets.all(10),
                        child: GestureDetector(
                          onTap: programProvider.programs[index].programId == 4
                              ? () {}
                              : () {
                                  programProvider.setActiveProgram(
                                      programProvider.programs[index]);
                                },
                          child: Container(
                            decoration: BoxDecoration(
                              color: getProgramColor(
                                  programProvider.programs[index].programId),
                              borderRadius: BorderRadius.circular(4),
                              boxShadow: [
                                BoxShadow(
                                  color: getProgramColor(programProvider
                                          .programs[index].programId)
                                      .withOpacity(0.5),
                                  spreadRadius: 2,
                                  blurRadius: 7,
                                  offset: Offset(
                                      0, 3), // changes position of shadow
                                ),
                              ],
                            ),
                            padding: const EdgeInsets.symmetric(
                              vertical: 25,
                              horizontal: 10,
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                ClipOval(
                                  child: Container(
                                    width: 40,
                                    height: 40,
                                    color: Colors.white,
                                    child: buildProgramIcon(programProvider
                                        .programs[index].programId),
                                  ),
                                ),
                                SizedBox(height: 10),
                                Expanded(
                                  child: Text(
                                    programProvider.programs[index].programName,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w600,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                else
                  Shimmer.fromColors(
                      child: Column(
                        children: [
                          Container(
                            height: 70,
                            child: SizedBox.expand(
                              child: PrimaryButton(
                                color: Colors.white,
                                text: '',
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                          Container(
                            height: 70,
                            child: SizedBox.expand(
                              child: PrimaryButton(
                                color: Colors.white,
                                text: '',
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                          Container(
                            height: 70,
                            child: SizedBox.expand(
                              child: PrimaryButton(
                                color: Colors.white,
                                text: '',
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                          Container(
                            height: 70,
                            child: SizedBox.expand(
                              child: PrimaryButton(
                                color: Colors.white,
                                text: '',
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                        ],
                      ),
                      baseColor: Colors.grey.withOpacity(0.3),
                      highlightColor: Colors.white),
              ],
            )),
      ),
    );
  }
}
