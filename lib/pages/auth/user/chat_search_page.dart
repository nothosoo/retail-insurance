import 'package:flutter/material.dart';
import 'package:silicone/flat_icon_icons.dart';
import 'package:silicone/pages/auth/user/chat_user_list_page.dart';
import 'package:silicone/providers/auth_provider.dart';
import 'package:silicone/providers/program_provider.dart';
import 'package:silicone/providers/user_provider.dart';
import 'package:silicone/utils/constant.dart';
import 'package:provider/provider.dart';

class ChatSearchPage extends StatefulWidget {
  const ChatSearchPage({Key key}) : super(key: key);

  @override
  _ChatSearchPageState createState() => _ChatSearchPageState();
}

class _ChatSearchPageState extends State<ChatSearchPage> {
  TextEditingController search = TextEditingController();

  @override
  Widget build(BuildContext context) {
    ProgramProvider programProvider =
        Provider.of<ProgramProvider>(context, listen: false);
    UserProvider userProvider =
        Provider.of<UserProvider>(context, listen: false);
    AuthProvider authProvider =
        Provider.of<AuthProvider>(context, listen: false);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: programProvider.activeProgram.programId == 1
            ? PRIMARY_COLOR
            : programProvider.activeProgram.programId == 2
                ? SECONDARY_COLOR
                : programProvider.activeProgram.programId == 3
                    ? SUCCESS_COLOR
                    : GRAY_COLOR,
        title: Text('Хэрэглэгч хайх', style: TextStyle(fontSize: 14)),
      ),
      body: Container(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(20),
              child: TextFormField(
                onChanged: (val) {
                  setState(() {});
                },
                controller: search,
                autofocus: true,
                decoration: InputDecoration(
                  filled: true,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(4),
                    borderSide: BorderSide.none,
                  ),
                  hintText: 'Хайх',
                  contentPadding: const EdgeInsets.symmetric(
                    horizontal: 15,
                    vertical: 15,
                  ),
                  suffixIcon: Icon(FlatIcon.flaticonsearch1),
                ),
              ),
            ),
            ...userProvider.users
                .map(
                  (e) => UserListItem(
                    currentUser: authProvider.user,
                    user: e,
                    show: e.fullName.contains(
                        new RegExp(r'' + search.text, caseSensitive: false)),
                  ),
                )
                .toList(),
          ],
        ),
      ),
    );
  }
}
