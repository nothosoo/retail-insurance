import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:silicone/flat_icon_icons.dart';
import 'package:silicone/models/chat_user.dart';
import 'package:silicone/models/user.dart';
import 'package:silicone/pages/auth/user/chat_wrapper_page.dart';
import 'package:silicone/providers/notification_provider.dart';
import 'package:silicone/utils/constant.dart';
import 'package:provider/provider.dart';

const INPUT_HEIGHT = 60;
const HEADER_HEIGHT = 50;
const PADDING_HEIGHT = 30;

class ChatPage extends StatefulWidget {
  final ChatUser user;
  final User currentUser;
  final bool isNotification;

  const ChatPage({
    Key key,
    this.user,
    this.currentUser,
    this.isNotification = false,
  }) : super(key: key);

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  TextEditingController _chatController = TextEditingController();
  // final _chatAnimateKey = GlobalKey<AnimatedListState>();
  int currentId;
  int peerId;
  String groupChatId;

  CollectionReference users = FirebaseFirestore.instance.collection('users');
  CollectionReference messagesCollection =
      FirebaseFirestore.instance.collection('messages');

  @override
  void initState() {
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 1000),
    );
    _animationController.forward();
    currentId = widget.currentUser.userId;
    peerId = widget.user.managerId;
    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  void onSendMessage(
    String content,
    int type,
    NotificationProvider notificationProvider,
  ) {
    try {
      if (content.trim() != '') {
        _chatController.clear();
        var rootReference =
            FirebaseFirestore.instance.collection('messages').doc(groupChatId);
        var documentReference = FirebaseFirestore.instance
            .collection('messages')
            .doc(groupChatId)
            .collection(groupChatId)
            .doc(DateTime.now().millisecondsSinceEpoch.toString());

        FirebaseFirestore.instance.runTransaction((transaction) async {
          transaction.set(
            rootReference,
            {'id': groupChatId},
          );
        }).then((value) {
          FirebaseFirestore.instance.runTransaction((transaction) async {
            transaction.set(
              documentReference,
              {
                'idFrom': currentId,
                'idTo': peerId,
                'timestamp': DateTime.now().millisecondsSinceEpoch.toString(),
                'content': content,
                'type': type
              },
            );
          }).then((value) async {
            notificationProvider.sendNotification(
              id: widget.user.deviceToken,
              title: widget.user.fullName,
              idFrom: currentId,
              idTo: peerId,
              type: type,
              content: content,
            );
            await documentReference.get();
          });
        });
      }
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    NotificationProvider notificationProvider =
        Provider.of<NotificationProvider>(context);
    if (currentId.hashCode <= peerId.hashCode) {
      groupChatId = '$currentId-$peerId';
    } else {
      groupChatId = '$peerId-$currentId';
    }
    return WillPopScope(
      onWillPop: () async {
        if (widget.isNotification) {
          Navigator.push(
            context,
            PageRouteBuilder(
              pageBuilder: (c, a1, a2) => ChatWrapperPage(isNotification: true),
              transitionsBuilder: (c, anim, a2, child) =>
                  FadeTransition(opacity: anim, child: child),
              transitionDuration: Duration(milliseconds: 300),
            ),
          );
          return false;
        }
        return true;
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: Container(
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.all(15),
            child: Stack(
              children: [
                // Container(
                //   height: MediaQuery.of(context).size.height -
                //       (MediaQuery.of(context).padding.top) -
                //       (MediaQuery.of(context).padding.bottom) -
                //       HEADER_HEIGHT -
                //       PADDING_HEIGHT -
                //       INPUT_HEIGHT,

                Align(
                  alignment: Alignment.bottomLeft,
                  child: Container(
                    child: Column(
                      children: [
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.only(
                              bottom: 10,
                              top: HEADER_HEIGHT.toDouble(),
                            ),
                            child: StreamBuilder(
                              stream: FirebaseFirestore.instance
                                  .collection('messages')
                                  .doc(groupChatId)
                                  .collection(groupChatId)
                                  .orderBy('timestamp', descending: true)
                                  .snapshots(),
                              builder: (context, snapshot) {
                                if (!snapshot.hasData) {
                                  return Center(
                                      child: CircularProgressIndicator(
                                          valueColor:
                                              AlwaysStoppedAnimation<Color>(
                                                  PRIMARY_COLOR)));
                                } else {
                                  // listMessage = snapshot.data.documents;
                                  return AnimatedSwitcher(
                                    key: Key(
                                        snapshot.data.docs.length.toString()),
                                    duration: Duration(seconds: 1),
                                    child: ListView.builder(
                                      reverse: true,
                                      itemCount: snapshot.data.docs.length,
                                      itemBuilder: (context, index) {
                                        return ChatItem(
                                          message: snapshot.data.docs[index]
                                              ['content'],
                                          mine: snapshot.data.docs[index]
                                                  ['idFrom'] ==
                                              currentId,
                                          imageUrl: widget.user.profileImage,
                                        );
                                      },
                                    ),
                                  );
                                  // return AnimatedList(
                                  //   key: _chatAnimateKey,
                                  //   reverse: true,
                                  //   initialItemCount: snapshot.data.docs.length,
                                  //   itemBuilder: (context, index, animation) {
                                  //     return SlideTransition(
                                  //       position: animation.drive(Tween(
                                  //         begin: Offset(0.0, 1.0),
                                  //         end: Offset.zero,
                                  //       )),
                                  //       child: ChatItem(
                                  //         message: snapshot.data.docs[index]
                                  //             ['content'],
                                  //         mine: snapshot.data.docs[index]
                                  //                 ['idFrom'] ==
                                  //             currentId,
                                  //         imageUrl: widget.user.profileImage,
                                  //       ),
                                  //     );
                                  //   },
                                  // );
                                }
                              },
                            ),
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4),
                            color: Colors.white,
                            border: Border.all(
                                color: Colors.black.withOpacity(0.15)),
                            // boxShadow: [
                            //   BoxShadow(
                            //     color: Colors.black.withOpacity(0.15),
                            //     spreadRadius: 2,
                            //     blurRadius: 5,
                            //     offset:
                            //         Offset(0, 3), // changes position of shadow
                            //   ),
                            // ],
                          ),
                          child: Row(
                            children: [
                              Expanded(
                                child: TextFormField(
                                  maxLines: null,
                                  controller: _chatController,
                                  decoration: InputDecoration(
                                    contentPadding: const EdgeInsets.symmetric(
                                      vertical: 5,
                                      horizontal: 10,
                                    ),
                                    hintText: 'Энд бичнэ үү',
                                    hintStyle: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14,
                                    ),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide.none,
                                    ),
                                  ),
                                  style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 14,
                                  ),
                                ),
                              ),
                              IconButton(
                                icon: Icon(Icons.send, color: PRIMARY_COLOR),
                                onPressed: () {
                                  onSendMessage(
                                    _chatController.text,
                                    1,
                                    notificationProvider,
                                  );
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.topLeft,
                  child: Container(
                    height: double.parse(HEADER_HEIGHT.toString()),
                    padding: const EdgeInsets.only(bottom: 10),
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(color: GRAY_BACKGROUND_COLOR),
                      ),
                      color: Colors.white,
                    ),
                    child: Row(
                      children: [
                        IconButton(
                          onPressed: () async {
                            if (widget.isNotification)
                              Navigator.push(
                                context,
                                PageRouteBuilder(
                                  pageBuilder: (c, a1, a2) =>
                                      ChatWrapperPage(isNotification: true),
                                  transitionsBuilder: (c, anim, a2, child) =>
                                      FadeTransition(
                                          opacity: anim, child: child),
                                  transitionDuration:
                                      Duration(milliseconds: 300),
                                ),
                              );
                            else
                              Navigator.of(context).pop();
                          },
                          icon: Icon(FlatIcon.flaticonleftarrow, size: 16),
                        ),
                        SizedBox(width: 0),
                        Container(
                          width: 30,
                          height: 30,
                          child: Stack(
                            children: [
                              ClipOval(
                                child: CachedNetworkImage(
                                  imageUrl: widget.user.profileImage,
                                  placeholder: (context, url) =>
                                      CircularProgressIndicator(),
                                  errorWidget: (context, url, error) =>
                                      Icon(Icons.error),
                                ),
                              ),
                              Align(
                                alignment: Alignment.bottomRight,
                                child: ClipOval(
                                  child: Container(
                                    height: 9,
                                    width: 9,
                                    color: Colors.white,
                                    child: Center(
                                      child: ClipOval(
                                        child: Container(
                                          height: 6,
                                          width: 6,
                                          color: widget.user.status == '1'
                                              ? Colors.green[400]
                                              : widget.user.status == '0'
                                                  ? Colors.grey
                                                  : Colors.amber,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(width: 20),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.user.fullName,
                              style: TextStyle(fontWeight: FontWeight.w500),
                            ),
                            Text(
                              widget.user.typeName,
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w500,
                                color: Colors.black.withOpacity(0.5),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ChatItem extends StatelessWidget {
  final bool mine;
  final String message;
  final String imageUrl;

  const ChatItem({
    Key key,
    @required this.mine,
    @required this.message,
    @required this.imageUrl,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      padding: const EdgeInsets.only(bottom: 10),
      child: Row(
        mainAxisAlignment:
            mine ? MainAxisAlignment.end : MainAxisAlignment.start,
        children: [
          if (!mine)
            Container(
              height: 40,
              child: ClipOval(
                child: CachedNetworkImage(imageUrl: imageUrl),
              ),
            ),
          if (!mine) SizedBox(width: 10),
          Container(
            constraints: BoxConstraints(
              minWidth: 50,
              maxWidth: MediaQuery.of(context).size.width * 0.6,
            ),
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            decoration: BoxDecoration(
              color: mine ? PRIMARY_COLOR : GRAY_BACKGROUND_COLOR,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10),
                topRight: Radius.circular(10),
                bottomLeft: mine ? Radius.circular(10) : Radius.circular(0),
                bottomRight: mine ? Radius.circular(0) : Radius.circular(10),
              ),
            ),
            child: Text(
              message,
              style: TextStyle(
                color: mine ? Colors.white : null,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
