import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:silicone/components/text.dart';
import 'package:silicone/flat_icon_icons.dart';
import 'package:silicone/models/menu.dart';
import 'package:silicone/pages/auth/user/chat_wrapper_page.dart';
import 'package:silicone/providers/auth_provider.dart';
import 'package:silicone/providers/program_provider.dart';
import 'package:silicone/utils/constant.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:cached_network_image/cached_network_image.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  List<Menu> menus;
  String url;
  String title = '';
  String currentWeb = '';
  String accessToken;
  int programId;
  Map<String, String> webs = Map();
  List<String> expanded = [];
  List<Menu> histories = [];
  bool profileShow = false;

  setWebUrl(Menu menu, String token, int id, {bool isBack = false}) {
    accessToken = token;
    programId = id;
    url = buildUrl(menu, token);
    url = url.replaceFirst('&pId=&', '&pId=' + id.toString() + '&');
    if ((histories.firstWhere((e) => e.menuID == menu.menuID,
            orElse: () => null)) ==
        null) {
      histories.add(menu);
    }
    title = menu.title;
    currentWeb = menu.menuID;
    if (!webs.containsKey(menu.menuID)) webs[menu.menuID] = url;
    expanded = [];
    if (!isBack) Navigator.of(context).pop();
    setState(() {});
  }

  String buildUrl(Menu menu, String token, {bool isFirst = false}) {
    if (isFirst) {
      if (histories.isEmpty) histories.add(menu);
    }
    return APP_URL + menu.url + '&tkn=' + token;
  }

  @override
  void initState() {
    super.initState();
    // if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  Future<bool> _willPopCallback(AuthProvider authProvider) async {
    if (histories.isNotEmpty) {
      if (histories.length > 1) {
        histories.removeLast();
        webs.removeWhere((key, value) => key == histories.last.menuID);
        setWebUrl(histories.last, accessToken, programId, isBack: true);
      } else {
        askIfPop(authProvider);
      }
    } else {
      askIfPop(authProvider);
    }
    return false;
  }

  askIfPop(AuthProvider authProvider) {
    showDialog(
      context: context,
      builder: (context) {
        return SimpleDialog(
          contentPadding: const EdgeInsets.all(20),
          children: [
            Text(
              'Та гарахдаа итгэлтэй байна уу?',
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 18,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    'Үгүй',
                    style: TextStyle(
                      color: Colors.black.withOpacity(0.5),
                    ),
                  ),
                ),
                TextButton(
                  onPressed: () {
                    authProvider.logUserOut();
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    'Тийм',
                    style: TextStyle(
                      color: PRIMARY_COLOR,
                    ),
                  ),
                ),
              ],
            ),
          ],
        );
      },
    );
  }

  Widget buildOpenPageItem(Menu menu, int i) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: PRIMARY_COLOR),
          borderRadius: BorderRadius.circular(4),
        ),
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              menu.title,
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 12,
              ),
            ),
            if (i != 0)
              SizedBox(
                height: 15,
                width: 15,
                child: IconButton(
                  onPressed: () {
                    histories.removeWhere((e) => e.menuID == menu.menuID);
                    webs.removeWhere((key, value) => key == menu.menuID);
                    setWebUrl(histories.last, accessToken, programId,
                        isBack: false);
                  },
                  padding: const EdgeInsets.all(0),
                  icon: Icon(
                    FlatIcon.flaticoncross,
                    size: 12,
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    ProgramProvider programProvider = Provider.of<ProgramProvider>(context);
    AuthProvider authProvider =
        Provider.of<AuthProvider>(context, listen: false);
    menus = programProvider.activeProgram.menus;
    return WillPopScope(
      onWillPop: () async {
        return await _willPopCallback(authProvider);
      },
      child: Scaffold(
        appBar: AppBar(
          title: title == '' &&
                  programProvider.activeProgram.menus != null &&
                  programProvider.activeProgram.menus.isNotEmpty
              ? Text(
                  programProvider.activeProgram.menus[0].title,
                  style: TextStyle(fontSize: 14),
                  overflow: TextOverflow.fade,
                )
              : Text(
                  title,
                  style: TextStyle(fontSize: 14),
                  overflow: TextOverflow.fade,
                ),
          leading: Builder(
            builder: (context) => // Ensure Scaffold is in context
                IconButton(
                    icon: Icon(FlatIcon.flaticonbars2),
                    onPressed: () => Scaffold.of(context).openDrawer()),
          ),
          backgroundColor: programProvider.activeProgram.programId == 1
              ? PRIMARY_COLOR
              : programProvider.activeProgram.programId == 2
                  ? SECONDARY_COLOR
                  : programProvider.activeProgram.programId == 3
                      ? SUCCESS_COLOR
                      : GRAY_COLOR,
          elevation: 10,
          actions: [
            TextButton(
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (_) => SimpleDialog(
                    titlePadding: const EdgeInsets.only(
                      left: 15,
                      top: 20,
                      right: 15,
                    ),
                    title: Text('Нээлттэй хуудсууд'),
                    contentPadding: const EdgeInsets.all(15),
                    children: [
                      Container(
                        child: SingleChildScrollView(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              ...histories
                                  .asMap()
                                  .entries
                                  .map((e) => buildOpenPageItem(e.value, e.key))
                                  .toList(),
                              SizedBox(height: 5),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              },
              style: TextButton.styleFrom(
                padding: EdgeInsets.zero,
                minimumSize: Size(50, 30),
                alignment: Alignment.center,
              ),
              child: Icon(
                FlatIcon.flaticonlayers,
                color: Colors.white,
                size: 20,
              ),
            ),
            TextButton(
              onPressed: () {
                Navigator.push(
                  context,
                  PageRouteBuilder(
                    pageBuilder: (c, a1, a2) => ChatWrapperPage(),
                    transitionsBuilder: (c, anim, a2, child) =>
                        FadeTransition(opacity: anim, child: child),
                    transitionDuration: Duration(milliseconds: 300),
                  ),
                );
              },
              style: TextButton.styleFrom(
                padding: EdgeInsets.zero,
                minimumSize: Size(50, 30),
                alignment: Alignment.center,
              ),
              child: Icon(
                FlatIcon.flaticonchat1,
                color: Colors.white,
                size: 20,
              ),
            ),
          ],
          shadowColor: programProvider.activeProgram.programId == 1
              ? PRIMARY_COLOR
              : programProvider.activeProgram.programId == 2
                  ? SECONDARY_COLOR
                  : programProvider.activeProgram.programId == 3
                      ? SUCCESS_COLOR
                      : GRAY_COLOR,
        ),
        drawer: Drawer(
          child: Container(
            color: Colors.white,
            child: SingleChildScrollView(
              child: menus != null
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 50),
                        ExpansionPanelList(
                          elevation: 0,
                          expansionCallback: (int index, bool isExpanded) {
                            setState(() {
                              profileShow = !profileShow;
                            });
                          },
                          children: [
                            ExpansionPanel(
                              // backgroundColor:
                              //     profileShow ? Color(0xffffaf8f8) : null,
                              headerBuilder: (context, isExpanded) {
                                return Row(
                                  children: [
                                    //     child: Image(image: AssetImage('images/logo.png')),
                                    SizedBox(width: 30),
                                    ClipOval(
                                      child: Container(
                                        height: 50,
                                        width: 50,
                                        color: GRAY_BACKGROUND_COLOR,
                                        child: authProvider.user.userPhotoUrl !=
                                                null
                                            ? CachedNetworkImage(
                                                imageUrl: authProvider
                                                    .user.userPhotoUrl,
                                              )
                                            : Text(''),
                                      ),
                                    ),
                                    SizedBox(width: 10),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          authProvider.user.firstName,
                                          style: TextStyle(
                                            color: PRIMARY_COLOR,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                        SizedBox(height: 5),
                                        Text(
                                          authProvider.user.userTypeName,
                                          style: TextStyle(fontSize: 10),
                                        ),
                                      ],
                                    ),
                                  ],
                                );
                              },
                              body: Column(
                                children: [
                                  ListTile(
                                    dense: true,
                                    visualDensity: VisualDensity(
                                        horizontal: 0, vertical: -2),
                                    contentPadding: const EdgeInsets.symmetric(
                                      horizontal: 30,
                                      vertical: 0,
                                    ),
                                    onTap: () {},
                                    title: Text(
                                      'Миний тухай',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500,
                                        color: GRAY_TEXT_COLOR,
                                      ),
                                    ),
                                  ),
                                  ListTile(
                                    dense: true,
                                    visualDensity: VisualDensity(
                                        horizontal: 0, vertical: -2),
                                    contentPadding: const EdgeInsets.symmetric(
                                      horizontal: 30,
                                      vertical: 0,
                                    ),
                                    onTap: () {},
                                    title: Text(
                                      'Үүрэг даалгавар',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500,
                                        color: GRAY_TEXT_COLOR,
                                      ),
                                    ),
                                  ),
                                  ListTile(
                                    dense: true,
                                    visualDensity: VisualDensity(
                                        horizontal: 0, vertical: -2),
                                    contentPadding: const EdgeInsets.symmetric(
                                      horizontal: 30,
                                      vertical: 0,
                                    ),
                                    onTap: () {
                                      askIfPop(authProvider);
                                    },
                                    title: Text(
                                      'Гарах',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500,
                                        color: GRAY_TEXT_COLOR,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              isExpanded: profileShow,
                            ),
                          ],
                        ),
                        Divider(),
                        Container(
                          padding: const EdgeInsets.only(
                            right: 30,
                            left: 30,
                            bottom: 20,
                            top: 10,
                          ),
                          width: MediaQuery.of(context).size.width,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Модуль',
                                style: TextStyle(fontSize: 10),
                              ),
                              DropdownButton<String>(
                                elevation: 2,
                                isDense: true,
                                isExpanded: true,
                                style: TextStyle(
                                  fontSize: 12,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontFamily: 'Montserrat',
                                ),
                                value:
                                    programProvider.activeProgram.programName,
                                items: programProvider.programs.map((value) {
                                  return DropdownMenuItem(
                                    value: value.programName,
                                    child: Text(
                                      value.programName,
                                      style: TextStyle(
                                          color: 4 == value.programId
                                              ? Colors.black.withOpacity(0.5)
                                              : null),
                                    ),
                                  );
                                }).toList(),
                                onChanged: (_) {
                                  int i = programProvider.programs
                                      .indexWhere((e) => e.programName == _);
                                  if (programProvider.programs[i].programId !=
                                          4 &&
                                      programProvider.activeProgram.programId !=
                                          programProvider
                                              .programs[i].programId) {
                                    programProvider.setActiveProgram(
                                        programProvider.programs[i]);
                                    url = '';
                                    title = '';
                                    currentWeb = '';
                                    Navigator.of(context).pop();
                                  }
                                },
                              )
                            ],
                          ),
                        ),
                        ...menus
                            .map(
                              (e) => e.subMenus.isEmpty
                                  ? ListTile(
                                      contentPadding:
                                          const EdgeInsets.symmetric(
                                              horizontal: 30),
                                      leading: Icon(
                                        FlatIcons[e.icon],
                                        color: Color(0xfffa1a2a6),
                                        size: 20,
                                      ),
                                      dense: true,
                                      horizontalTitleGap: 5,
                                      title: Text(
                                        e.title,
                                        style: TextStyle(
                                          color: GRAY_TEXT_COLOR,
                                          fontSize: 12,
                                        ),
                                      ),
                                      onTap: () {
                                        setWebUrl(
                                            e,
                                            authProvider.user.accessToken,
                                            programProvider
                                                .activeProgram.programId);
                                      },
                                    )
                                  : Theme(
                                      data: ThemeData(
                                        fontFamily: 'Montserrat',
                                      ).copyWith(
                                        dividerColor: Colors.transparent,
                                      ),
                                      child: ListTileTheme(
                                        tileColor: expanded.contains(e.menuID)
                                            ? Color(0xffff2f2f2)
                                            : null,
                                        contentPadding: EdgeInsets.all(0),
                                        dense: true,
                                        horizontalTitleGap: 5,
                                        child: ExpansionTile(
                                          onExpansionChanged: (value) {
                                            if (value) {
                                              expanded.add(e.menuID);
                                            } else {
                                              expanded.remove(e.menuID);
                                            }
                                            setState(() {});
                                          },
                                          tilePadding:
                                              const EdgeInsets.symmetric(
                                                  horizontal: 30),
                                          leading: Icon(
                                            FlatIcons[e.icon],
                                            color: Color(0xfffa1a2a6),
                                            size: 20,
                                          ),
                                          title: Text(
                                            e.title,
                                            style: TextStyle(
                                              color: GRAY_TEXT_COLOR,
                                              fontSize: 12,
                                              fontWeight: FontWeight.w500,
                                            ),
                                          ),
                                          children: [
                                            ...e.subMenus
                                                .map((e) => ListTile(
                                                      contentPadding:
                                                          const EdgeInsets.only(
                                                              left: 30),
                                                      minVerticalPadding: 0,
                                                      tileColor:
                                                          Color(0xffffaf8f8),
                                                      title: Text(
                                                        '- ' + e.title,
                                                        style: TextStyle(
                                                          color:
                                                              GRAY_TEXT_COLOR,
                                                          fontSize: 12,
                                                          fontWeight:
                                                              FontWeight.w500,
                                                        ),
                                                      ),
                                                      onTap: () {
                                                        setWebUrl(
                                                            e,
                                                            authProvider.user
                                                                .accessToken,
                                                            programProvider
                                                                .activeProgram
                                                                .programId);
                                                      },
                                                    ))
                                                .toList()
                                          ],
                                        ),
                                      ),
                                    ),
                            )
                            .toList()
                      ],
                    )
                  : Text(''),
            ),
          ),
        ),
        body: SafeArea(
          child: Container(
            child: programProvider.activeProgram.menus == null
                ? Text('')
                : programProvider.activeProgram.menus.isNotEmpty &&
                        currentWeb == ''
                    ? Stack(
                        children: [
                          Center(
                              child: Image(
                                  image: AssetImage('images/loading.gif'))),
                          PageViewer(
                            url: buildUrl(
                              programProvider.activeProgram.menus[0],
                              authProvider.user.accessToken,
                              isFirst: true,
                            ),
                            show: true,
                            parentContext: context,
                          ),
                        ],
                      )
                    : Stack(
                        children: [
                          Center(
                              child: Image(
                                  image: AssetImage('images/loading.gif'))),
                          SingleChildScrollView(
                            physics: const NeverScrollableScrollPhysics(),
                            child: Column(
                              children: [
                                ...webs.entries
                                    .map(
                                      (e) => PageViewer(
                                        url: e.value,
                                        show: e.key == currentWeb,
                                        parentContext: context,
                                      ),
                                    )
                                    .toList(),
                              ],
                            ),
                          ),
                        ],
                      ),
          ),
        ),
      ),
    );
  }
}

class PageViewer extends StatefulWidget {
  final String url;
  final bool show;
  final BuildContext parentContext;

  const PageViewer({
    Key key,
    this.url,
    this.show,
    @required this.parentContext,
  }) : super(key: key);

  @override
  _PageViewerState createState() => _PageViewerState();
}

class _PageViewerState extends State<PageViewer>
    with AutomaticKeepAliveClientMixin {
  int status = 0;
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return AnimatedOpacity(
      duration: Duration(milliseconds: 500),
      opacity: widget.show != null && widget.show && status == 100 ? 1.0 : 0.0,
      child: Container(
        height: widget.show != null && widget.show
            ? MediaQuery.of(widget.parentContext).size.height -
                AppBar().preferredSize.height -
                MediaQuery.of(widget.parentContext).padding.top -
                MediaQuery.of(widget.parentContext).padding.bottom
            : 0,
        width: MediaQuery.of(context).size.width,
        child: widget.url != null
            ? WebView(
                key: Key(widget.url),
                initialUrl: widget.url,
                javascriptMode: JavascriptMode.unrestricted,
                onProgress: (int progress) {
                  setState(() {
                    print(progress);
                    status = progress;
                  });
                },
              )
            : Text(''),
      ),
    );
  }
}
