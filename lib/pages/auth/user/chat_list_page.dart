import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:silicone/components/text.dart';
import 'package:silicone/models/chat_user.dart';
import 'package:silicone/models/user.dart';
import 'package:silicone/pages/auth/user/chat_page.dart';
import 'package:silicone/utils/constant.dart';
import 'package:intl/intl.dart';

class ChatListPage extends StatefulWidget {
  final List<ChatUser> users;
  final User user;

  const ChatListPage({Key key, this.users, this.user}) : super(key: key);
  @override
  _ChatListPageState createState() => _ChatListPageState();
}

class _ChatListPageState extends State<ChatListPage> {
  Widget buildListItem(int idTo, int idFrom, String text, String timestamp) {
    ChatUser tmpUser;
    if (widget.user != null &&
        widget.user.userId == idTo &&
        widget.users != null &&
        widget.users.isNotEmpty) {
      tmpUser = widget.users.firstWhere((e) => e.managerId == idFrom);
    } else if (widget.user != null && widget.user.userId == idFrom) {
      tmpUser = widget.users.firstWhere((e) => e.managerId == idTo);
    }
    if (tmpUser != null)
      return GestureDetector(
        onTap: () {
          Navigator.push(
            context,
            PageRouteBuilder(
              pageBuilder: (c, a1, a2) => ChatPage(
                user: tmpUser,
                currentUser: widget.user,
              ),
              transitionsBuilder: (c, anim, a2, child) =>
                  FadeTransition(opacity: anim, child: child),
              transitionDuration: Duration(milliseconds: 300),
            ),
          );
        },
        child: ChatListItem(
          name: tmpUser.fullName,
          profileUrl: tmpUser.profileImage,
          dateTime: timestamp,
          text: text,
          status: tmpUser.status,
        ),
      );
    else
      return Text('');
  }

  @override
  void initState() {
    super.initState();
  }

  List<QueryDocumentSnapshot> getOwnConversations(
      List<QueryDocumentSnapshot> chats) {
    List<QueryDocumentSnapshot> filtered = [];
    chats.forEach((e) {
      List<String> ids = e['id'].split('-');
      if (widget.user != null) if (int.parse(ids[0]) == widget.user.userId ||
          int.parse(ids[1]) == widget.user.userId) {
        filtered.add(e);
      }
    });
    return filtered;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: StreamBuilder(
        stream: FirebaseFirestore.instance.collection('messages').snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(
                child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(PRIMARY_COLOR)));
          } else if (snapshot.data.docs.length == 0) {
            return Center(
                child: Text(
              'Танд чат байхгүй байна',
              style: TextStyle(
                color: Colors.black.withOpacity(0.5),
                fontWeight: FontWeight.w500,
              ),
            ));
          } else {
            List<QueryDocumentSnapshot> ownConversations =
                getOwnConversations(snapshot.data.docs);
            return ListView.builder(
              itemCount: ownConversations.length,
              itemBuilder: (context, i) {
                return FutureBuilder<QuerySnapshot>(
                  future: FirebaseFirestore.instance
                      .collection('messages')
                      .doc(ownConversations[i]['id'])
                      .collection(ownConversations[i]['id'])
                      .get(),
                  builder: (BuildContext context,
                      AsyncSnapshot<QuerySnapshot> snapshot) {
                    if (snapshot.hasError) {
                      return Center(
                        child: MutedText(
                          text: 'Алдаа гарлаа',
                        ),
                      );
                    }

                    // if (!snapshot.hasData) {
                    //   return Center(
                    //     child: MutedText(
                    //       text: 'Танд чат байхгүй байна',
                    //     ),
                    //   );
                    // }

                    if (snapshot.connectionState == ConnectionState.done) {
                      return buildListItem(
                        snapshot.data.docs[snapshot.data.docs.length - 1]
                            ['idTo'],
                        snapshot.data.docs[snapshot.data.docs.length - 1]
                            ['idFrom'],
                        snapshot.data.docs[snapshot.data.docs.length - 1]
                            ['content'],
                        snapshot.data.docs[snapshot.data.docs.length - 1]
                            ['timestamp'],
                      );
                    }

                    return Text('');
                  },
                );
              },
            );
          }
        },
      ),
    );
  }
}

class ChatListItem extends StatelessWidget {
  final String text;
  final String name;
  final String dateTime;
  final String profileUrl;
  final String status;

  const ChatListItem({
    Key key,
    this.text,
    this.name,
    this.dateTime,
    this.profileUrl,
    this.status,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final DateFormat format = DateFormat.Hm();
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Row(children: [
              Container(
                width: 50,
                height: 50,
                child: Stack(
                  children: [
                    ClipOval(
                      child: CachedNetworkImage(
                        imageUrl: profileUrl,
                        placeholder: (context, url) =>
                            CircularProgressIndicator(),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomRight,
                      child: ClipOval(
                        child: Container(
                          height: 14,
                          width: 14,
                          color: Colors.white,
                          child: Center(
                            child: ClipOval(
                              child: Container(
                                height: 10,
                                width: 10,
                                color: status == '1'
                                    ? Colors.green[400]
                                    : status == '0'
                                        ? Colors.grey
                                        : Colors.amber,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(width: 20),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      name,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 10),
                    Text(
                      text,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
              ),
            ]),
          ),
          SizedBox(width: 10),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  format.format(
                      DateTime.fromMillisecondsSinceEpoch(int.parse(dateTime))),
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                    color: Colors.black.withOpacity(0.5),
                  ),
                ),
                SizedBox(height: 10),
                ClipOval(
                  child: Container(
                    height: 20,
                    width: 20,
                    color: Colors.white,
                    child: Center(
                        child: Text(
                      '1',
                      style: TextStyle(
                        fontSize: 10,
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                      ),
                    )),
                  ),
                ),
              ],
            ),
          ),
          // Icon(FlatIcon.flaticonrightarrow, size: 14),
        ],
      ),
    );
  }
}
