import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:silicone/flat_icon_icons.dart';
import 'package:silicone/pages/auth/user/chat_list_page.dart';
import 'package:silicone/pages/auth/user/chat_search_page.dart';
import 'package:silicone/pages/wrapper.dart';
import 'package:silicone/providers/auth_provider.dart';
import 'package:silicone/providers/program_provider.dart';
import 'package:silicone/providers/user_provider.dart';
import 'package:silicone/utils/constant.dart';

class ChatWrapperPage extends StatefulWidget {
  final bool isNotification;

  const ChatWrapperPage({Key key, this.isNotification = false})
      : super(key: key);
  @override
  _ChatWrapperPageState createState() => _ChatWrapperPageState();
}

class _ChatWrapperPageState extends State<ChatWrapperPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ProgramProvider programProvider = Provider.of<ProgramProvider>(context);
    UserProvider userProvider = Provider.of<UserProvider>(context);
    AuthProvider authProvider =
        Provider.of<AuthProvider>(context, listen: false);
    return WillPopScope(
      onWillPop: () async {
        if (widget.isNotification) {
          Navigator.push(
            context,
            PageRouteBuilder(
              pageBuilder: (c, a1, a2) => Wrapper(isNotification: true),
              transitionsBuilder: (c, anim, a2, child) =>
                  FadeTransition(opacity: anim, child: child),
              transitionDuration: Duration(milliseconds: 300),
            ),
          );
          return false;
        }
        return true;
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: programProvider.activeProgram == null
              ? PRIMARY_COLOR
              : programProvider.activeProgram.programId == 1
                  ? PRIMARY_COLOR
                  : programProvider.activeProgram.programId == 2
                      ? SECONDARY_COLOR
                      : programProvider.activeProgram.programId == 3
                          ? SUCCESS_COLOR
                          : GRAY_COLOR,
          title: Text('Чат', style: TextStyle(fontSize: 14)),
          leading: IconButton(
            icon: Icon(FlatIcon.flaticonleftarrow),
            onPressed: () async {
              if (widget.isNotification)
                Navigator.push(
                  context,
                  PageRouteBuilder(
                    pageBuilder: (c, a1, a2) => Wrapper(isNotification: true),
                    transitionsBuilder: (c, anim, a2, child) =>
                        FadeTransition(opacity: anim, child: child),
                    transitionDuration: Duration(milliseconds: 300),
                  ),
                );
              else
                Navigator.of(context).pop();
            },
          ),
        ),
        body: Container(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(15),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  padding: const EdgeInsets.all(5),
                  child: TextFormField(
                    onTap: () {
                      Navigator.push(
                        context,
                        PageRouteBuilder(
                          pageBuilder: (c, a1, a2) => ChatSearchPage(),
                          transitionsBuilder: (c, anim, a2, child) =>
                              FadeTransition(opacity: anim, child: child),
                          transitionDuration: Duration(milliseconds: 300),
                        ),
                      );
                    },
                    readOnly: true,
                    decoration: InputDecoration(
                      filled: true,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(4),
                        borderSide: BorderSide.none,
                      ),
                      hintText: 'Хайх',
                      contentPadding: const EdgeInsets.symmetric(
                        horizontal: 15,
                        vertical: 15,
                      ),
                      suffixIcon: Icon(FlatIcon.flaticonsearch1),
                    ),
                  ),
                  // decoration: BoxDecoration(
                  //     border: Border(
                  //   bottom: BorderSide(
                  //       width: 1, color: Colors.black.withOpacity(0.1)),
                  // )),
                ),
              ),
              Expanded(
                child: ChatListPage(
                    users: userProvider.users, user: authProvider.user),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
