import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:silicone/models/chat_user.dart';
import 'package:silicone/models/user.dart';
import 'package:silicone/pages/auth/user/chat_page.dart';
import 'package:silicone/providers/auth_provider.dart';

class ChatUserListPage extends StatelessWidget {
  final List<ChatUser> users;
  ChatUserListPage({this.users});

  @override
  Widget build(BuildContext context) {
    AuthProvider authProvider =
        Provider.of<AuthProvider>(context, listen: false);

    return Container(
      child: authProvider.user != null
          ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ...users
                    .map(
                      (e) => UserListItem(
                        currentUser: authProvider.user,
                        user: e,
                      ),
                    )
                    .toList(),
              ],
            )
          : Center(child: CircularProgressIndicator()),
    );
  }
}

class UserListItem extends StatelessWidget {
  final ChatUser user;
  final User currentUser;
  final bool show;
  UserListItem({this.user, this.currentUser, this.show});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ChatPage(
              user: user,
              currentUser: currentUser,
            ),
          ),
        );
      },
      child: Container(
        height: show ? null : 0,
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        color: Colors.white,
        child: Row(
          children: [
            Container(
              width: 50,
              height: 50,
              child: Stack(
                children: [
                  ClipOval(
                    child: CachedNetworkImage(
                      imageUrl: user.profileImage,
                      placeholder: (context, url) =>
                          CircularProgressIndicator(),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomRight,
                    child: ClipOval(
                      child: Container(
                        height: 14,
                        width: 14,
                        color: Colors.white,
                        child: Center(
                          child: ClipOval(
                            child: Container(
                              height: 10,
                              width: 10,
                              color: user.status == '1'
                                  ? Colors.green[400]
                                  : user.status == '0'
                                      ? Colors.grey
                                      : Colors.amber,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(width: 20),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(user.fullName,
                    style: TextStyle(fontWeight: FontWeight.bold)),
                SizedBox(height: 5),
                Text(
                  user.typeName,
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 12,
                    color: Colors.black.withOpacity(0.5),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
