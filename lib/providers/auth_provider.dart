import 'dart:convert';
import 'dart:io';

import 'package:crypto/crypto.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:wifi/wifi.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:silicone/models/user.dart';
import 'package:silicone/services/api.dart';
import 'package:silicone/services/localUser.dart';
import 'package:silicone/utils/constant.dart';
import 'dart:async';

class AuthProvider with ChangeNotifier {
  static Api api;
  static bool isTicking;
  static String deviceName;
  static String deviceIp;
  static String firebaseToken;
  bool loggedIn = false;
  bool isBusy = false;
  User user;
  final _storage = FlutterSecureStorage();
  // GlobalKey<NavigatorState> _key;
  AuthProvider(key) {
    // _key = key;
    api = Api();
    checkUser();
    setDeviceInfo();
  }

  setDeviceInfo() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    try {
      if (Platform.isAndroid) {
        AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
        deviceName = androidInfo.model;
        print('Running on ${androidInfo.model}'); // e.g. "Moto G (4)"
      } else if (Platform.isIOS) {
        IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
        deviceName = iosInfo.name;
        print('Running on ${iosInfo.name}');
      }
    } catch (e) {
      debugPrint(e);
    }
    try {
      deviceIp = await Wifi.ip;
      FlutterSecureStorage storage = FlutterSecureStorage();
      String fToken = await storage.read(key: STORAGE_KEY_FIREBASE_TOKEN);
      firebaseToken = fToken;
    } catch (e) {
      debugPrint(e);
    }
  }

  checkUser() async {
    isBusy = true;
    notifyListeners();
    User tmp = await getLocalUser();
    if (tmp is User) {
      api = Api(token: tmp.accessToken);
      var res = await api.post(
        API_URL + 'UserLogin/GetUserByAccessToken',
        {'accessToken': tmp.accessToken},
      );
      if (res.data['userTypeName'] == null) {
        isBusy = false;
        logUserOut();
      } else {
        user = tmp;
        loggedIn = true;
        isBusy = false;
        _startRefreshTicker();
      }
      notifyListeners();
    }
    isBusy = false;
    notifyListeners();
  }

  logUserOut() {
    user = null;
    loggedIn = false;
    removeLocalUser();
    notifyListeners();
  }

  _startRefreshTicker() {
    isTicking = true;
    Timer.periodic(new Duration(minutes: 5), (timer) {
      try {
        debugPrint('____ticking');
        debugPrint(user.accessToken);
        api.post(API_URL + 'UserLogin/RefreshToken',
            {'refreshToken': user.refreshToken}).then((res) async {
          if (res != null) {
            if (res.data != null) {
              if (res.data['accessToken'] != null) {
                user.accessToken = res.data['accessToken'];
                await setLocalUserAccessToken(user.accessToken);
                notifyListeners();
                return;
              }
            }
          }
          logUserOut();
        });
      } catch (e) {
        debugPrint(e.toString());
      }
    });
  }

  Future login(username, password) async {
    try {
      var bytes1 = utf8.encode(password);
      var digest1 = sha256.convert(bytes1);
      String hash = digest1.toString();
      hash = hash.substring(0, 30);
      Api api = Api();
      print({
        'UserName': username,
        'Password': hash,
        'IpAddress': deviceIp,
        'DeviceName': deviceName,
        'DeviceToken': firebaseToken,
      });
      if (deviceIp == '') deviceIp = 'null';
      if (deviceName == '') deviceName = 'null';
      if (firebaseToken == null) firebaseToken = 'null';
      var res = await api.post(API_URL + 'UserLogin', {
        'UserName': username,
        'Password': hash,
        'IpAddress': deviceIp,
        'DeviceName': deviceName,
        'DeviceToken': firebaseToken,
      });
      if (res != null) {
        user = User.fromJson(res.data);
        loggedIn = true;
        //Save to device
        _storage.write(key: 'user', value: jsonEncode(res.data));
        _startRefreshTicker();
      }
      notifyListeners();
      return user;
    } catch (e) {
      print(e);
    }
  }
}
