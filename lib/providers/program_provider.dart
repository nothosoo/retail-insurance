import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:silicone/models/menu.dart';
import 'package:silicone/models/program.dart';
import 'package:silicone/models/user.dart';
import 'package:silicone/services/api.dart';
import 'package:silicone/services/localUser.dart';
import 'package:silicone/utils/constant.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class ProgramProvider with ChangeNotifier {
  List<Program> programs = [];
  Program activeProgram;
  Api _api;
  bool isBusy = false;
  ProgramProvider() {
    fetch();
  }

  setActiveProgram(Program p) async {
    activeProgram = p;
    FlutterSecureStorage storage = FlutterSecureStorage();
    storage.write(key: 'activeProgram', value: p.programId.toString());
    if (p != null) fetchMenu();
    notifyListeners();
  }

  Future<int> _checkLocalProgram() async {
    FlutterSecureStorage storage = FlutterSecureStorage();
    String localId = await storage.read(key: 'activeProgram');
    if (localId != null) {
      return int.parse(localId);
    }
    return null;
  }

  fetchMenu() async {
    var res = await _api.get(
        API_URL + 'sysMenus/AppLoad/' + activeProgram.programId.toString());
    if (res != null) {
      activeProgram.menus = [];
      List<Menu> tmp = [];
      for (var p in jsonDecode(res.data)) {
        tmp.add(Menu.fromJson(p));
        if (p['ParentID'] == MAIN_MENU_ID) {
          activeProgram.menus.add(Menu.fromJson(p));
        }
      }
      for (var p in activeProgram.menus) {
        for (var tp in tmp) {
          if (p.menuID == tp.parentID) {
            p.subMenus.add(tp);
          }
        }
      }
      notifyListeners();
    }
  }

  fetch() async {
    isBusy = true;
    notifyListeners();
    User user = await getLocalUser();
    _api = Api(token: user.accessToken);
    var res = await _api.get(API_URL + 'sysMenus/AllPrograms');
    if (res != null) {
      for (var p in jsonDecode(res.data)) {
        programs.add(Program.fromJson(p));
      }
      int localId = await _checkLocalProgram();
      if (localId != null) {
        Program p = programs.firstWhere((e) => e.programId == localId,
            orElse: () => null);
        print(p);
        if (p != null) {
          setActiveProgram(p);
        }
      }
    } else {
      programs = [];
      isBusy = false;
      notifyListeners();
      return null;
    }
    isBusy = false;
    notifyListeners();
  }
}
