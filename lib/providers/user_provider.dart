import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:silicone/models/chat_user.dart';
import 'package:silicone/models/user.dart';
import 'package:silicone/services/localUser.dart';
import 'package:silicone/utils/constant.dart';
import 'package:silicone/services/api.dart';
import 'dart:async';

class UserProvider with ChangeNotifier {
  static Api api;
  static bool isTicking;
  List<ChatUser> users = [];
  ChatUser user;

  UserProvider() {
    api = Api();
    fetch();
    _startTickerFetch();
  }

  _startTickerFetch() {
    isTicking = true;
    Timer.periodic(new Duration(minutes: 5), (timer) async {
      try {
        await fetch();
      } catch (e) {
        debugPrint(e.toString());
      }
    });
  }

  fetch() async {
    User tmp = await getLocalUser();
    api = Api(token: tmp.accessToken);
    var res = await api.get(API_URL + 'misUsers/Managers');
    if (res != null) {
      users = [];
      for (var p in res.data) {
        if (tmp.userId != p['managerId']) users.add(ChatUser.fromJson(p));
      }
      notifyListeners();
    } else {
      if (!isTicking) users = [];
      return null;
    }
    FirebaseFirestore.instance.collection('users').get().then(
      (QuerySnapshot querySnapshot) {
        if (querySnapshot.size == 0) {
          users.forEach((e) {
            FirebaseFirestore.instance.collection('users').add(e.toJson());
          });
        } else {
          users.forEach((u) {
            QueryDocumentSnapshot res = querySnapshot.docs.firstWhere(
              (e) => e['managerId'] == u.managerId,
              orElse: () => null,
            );
            if (res == null) {
              FirebaseFirestore.instance.collection('users').add(u.toJson());
            }
          });
        }
        notifyListeners();
      },
    );
  }

  ChatUser getUserById(id) {
    if (id is String)
      return users.firstWhere((e) => e.managerId == int.parse(id));
    else
      return users.firstWhere((e) => e.managerId == id);
  }
}
