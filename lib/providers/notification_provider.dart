import 'dart:math';

import 'package:flutter/material.dart';
import 'package:silicone/services/api.dart';

class NotificationProvider with ChangeNotifier {
  Api _api;
  Map<int, int> chats;

  NotificationProvider() {
    chats = {};
    init();
  }

  init() {
    _api = Api(
        firebaseKey:
            'key=AAAA_DaCqa4:APA91bFAcTW6-GJx28FfWIktqZrcdKlHb1OgpwbX6PUoTu4guDK8LYkfAtQWtd01tbQ2p8Zmv6SNigRbldVx2idye8uV8NsNReyqy17Jg7ADToMmog3Ve17cWpIxsBP66GzFsBQRPwlj');
  }

  _notifyFirebase({
    @required id,
    @required idFrom,
    @required idTo,
    @required type,
    @required title,
    @required content,
  }) {
    _api.post('https://fcm.googleapis.com/fcm/send', {
      "registration_ids": [id],
      "priority": "high",
      "data": {
        "clickaction": "FLUTTERNOTIFICATIONCLICK",
        "body": {
          "title": title,
          "idFrom": idFrom,
          "idTo": idTo,
          "type": type,
          "content": content
        },
        "id": Random().nextInt(5),
        "event": "CHAT"
      }
    });
  }

  sendNotification({
    @required id,
    @required title,
    @required idFrom,
    @required idTo,
    @required type,
    @required content,
  }) {
    int now = DateTime.now().millisecondsSinceEpoch;
    if (chats[idTo] != null) {
      if (chats[idTo] < now) {
        _notifyFirebase(
          id: id,
          title: title,
          content: content,
          idFrom: idFrom,
          idTo: idTo,
          type: type,
        );
        chats[idTo] =
            DateTime.now().add(Duration(minutes: 5)).millisecondsSinceEpoch;
      }
    } else {
      _notifyFirebase(
        id: id,
        title: title,
        content: content,
        idFrom: idFrom,
        idTo: idTo,
        type: type,
      );
      chats[idTo] =
          DateTime.now().add(Duration(minutes: 5)).millisecondsSinceEpoch;
    }
  }
}
