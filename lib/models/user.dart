class User {
  String userTypeName;
  int userId;
  int departmentId;
  int brokerId;
  int categoryId;
  String lastName;
  String firstName;
  String userName;
  String userPhotoUrl;
  String themeColor;
  String isHomeView;
  String accessToken;
  String refreshToken;
  List<String> sysShortcuts;

  User({
    this.userTypeName,
    this.userId,
    this.departmentId,
    this.brokerId,
    this.categoryId,
    this.lastName,
    this.firstName,
    this.userName,
    this.userPhotoUrl,
    this.themeColor,
    this.isHomeView,
    this.accessToken,
    this.refreshToken,
    this.sysShortcuts,
  });

  User.fromJson(Map<String, dynamic> json) {
    userTypeName = json['userTypeName'];
    userId = json['userId'];
    departmentId = json['departmentId'];
    brokerId = json['brokerId'];
    categoryId = json['categoryId'];
    lastName = json['lastName'];
    firstName = json['firstName'];
    userName = json['userName'];
    userPhotoUrl = json['userPhotoUrl'];
    themeColor = json['themeColor'];
    isHomeView = json['isHomeView'];
    accessToken = json['accessToken'];
    refreshToken = json['refreshToken'];
    // if (json['sysShortcuts'] != null) {
    //   sysShortcuts = [];
    //   json['sysShortcuts'].forEach((v) {
    //     sysShortcuts.add(new String.fromJson(v));
    //   });
    // }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userTypeName'] = this.userTypeName;
    data['userId'] = this.userId;
    data['departmentId'] = this.departmentId;
    data['brokerId'] = this.brokerId;
    data['categoryId'] = this.categoryId;
    data['lastName'] = this.lastName;
    data['firstName'] = this.firstName;
    data['userName'] = this.userName;
    data['userPhotoUrl'] = this.userPhotoUrl;
    data['themeColor'] = this.themeColor;
    data['isHomeView'] = this.isHomeView;
    data['accessToken'] = this.accessToken;
    data['refreshToken'] = this.refreshToken;
    // if (this.sysShortcuts != null) {
    //   data['sysShortcuts'] = this.sysShortcuts.map((v) => v.toJson()).toList();
    // }
    return data;
  }
}
