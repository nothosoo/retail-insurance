class ChatUser {
  int managerId;
  String userCode;
  String fullName;
  String phone;
  String profileImage;
  String status;
  String typeName;
  String deviceToken;

  ChatUser({
    this.managerId,
    this.userCode,
    this.fullName,
    this.phone,
    this.profileImage,
    this.status,
    this.typeName,
    this.deviceToken,
  });

  ChatUser.fromJson(Map<String, dynamic> json) {
    managerId = json['managerId'];
    userCode = json['userCode'];
    fullName = json['fullName'];
    phone = json['phone'];
    profileImage = json['profileImage'];
    status = json['status'];
    typeName = json['typeName'];
    deviceToken = json['deviceToken'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['managerId'] = this.managerId;
    data['userCode'] = this.userCode;
    data['fullName'] = this.fullName;
    data['phone'] = this.phone;
    data['profileImage'] = this.profileImage;
    data['status'] = this.status;
    data['typeName'] = this.typeName;
    data['deviceToken'] = this.deviceToken;
    return data;
  }
}
