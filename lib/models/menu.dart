class Menu {
  String menuID;
  String parentID;
  String title;
  String icon;
  String url;
  List<Menu> subMenus = [];

  Menu({
    this.menuID,
    this.parentID,
    this.title,
    this.icon,
    this.url,
  });

  Menu.fromJson(Map<String, dynamic> json) {
    menuID = json['MenuID'];
    parentID = json['ParentID'];
    title = json['Title'];
    icon = json['Icon'];
    url = json['Url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['MenuID'] = this.menuID;
    data['ParentID'] = this.parentID;
    data['Title'] = this.title;
    data['Icon'] = this.icon;
    data['Url'] = this.url;
    return data;
  }
}
