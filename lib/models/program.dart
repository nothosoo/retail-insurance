import 'package:silicone/models/menu.dart';

class Program {
  int programId;
  String programName;
  String backgroundColor;
  String textColor;
  String isMain;
  String homeClass;
  String homeIcon;
  String defaultMenuID;
  String defaultControllerId;
  String isView;
  List<Menu> menus;

  Program({
    this.programId,
    this.programName,
    this.backgroundColor,
    this.textColor,
    this.isMain,
    this.homeClass,
    this.homeIcon,
    this.defaultMenuID,
    this.defaultControllerId,
    this.isView,
  });

  Program.fromJson(Map<String, dynamic> json) {
    programId = json['ProgramId'];
    programName = json['ProgramName'];
    backgroundColor = json['BackgroundColor'];
    textColor = json['TextColor'];
    isMain = json['IsMain'];
    homeClass = json['HomeClass'];
    homeIcon = json['HomeIcon'];
    defaultMenuID = json['DefaultMenuID'];
    defaultControllerId = json['DefaultControllerId'];
    isView = json['IsView'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ProgramId'] = this.programId;
    data['ProgramName'] = this.programName;
    data['BackgroundColor'] = this.backgroundColor;
    data['TextColor'] = this.textColor;
    data['IsMain'] = this.isMain;
    data['HomeClass'] = this.homeClass;
    data['HomeIcon'] = this.homeIcon;
    data['DefaultMenuID'] = this.defaultMenuID;
    data['DefaultControllerId'] = this.defaultControllerId;
    data['IsView'] = this.isView;
    return data;
  }
}
