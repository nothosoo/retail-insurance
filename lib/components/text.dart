import 'package:flutter/material.dart';

class Header1 extends StatelessWidget {
  final String text;

  const Header1({Key key, this.text}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        fontSize: 32,
        fontWeight: FontWeight.bold,
      ),
    );
  }
}

class Header2 extends StatelessWidget {
  final String text;
  final TextAlign align;
  final Color color;

  const Header2({
    Key key,
    this.text,
    this.align,
    this.color,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        fontSize: 24,
        fontWeight: FontWeight.bold,
        color: color,
      ),
      textAlign: align,
    );
  }
}

class Header3 extends StatelessWidget {
  final String text;
  final TextAlign align;

  const Header3({Key key, this.text, this.align}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        fontSize: 18,
        fontWeight: FontWeight.bold,
      ),
      textAlign: align,
    );
  }
}

class MutedText extends StatelessWidget {
  final String text;

  const MutedText({Key key, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(text, style: TextStyle(color: Colors.grey));
  }
}
