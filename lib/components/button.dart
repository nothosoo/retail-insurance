import 'package:flutter/material.dart';
import 'package:silicone/components/loader.dart';
import 'package:silicone/utils/constant.dart';

class PrimaryButton extends StatelessWidget {
  final String text;
  final Function onPressed;
  final bool loading;
  final Color color;
  final IconData icon;

  const PrimaryButton({
    Key key,
    this.text,
    this.onPressed,
    this.loading,
    this.icon,
    @required this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ButtonStyle(
        elevation: MaterialStateProperty.all(0),
        padding: MaterialStateProperty.all(
          EdgeInsets.symmetric(horizontal: 30, vertical: 15),
        ),
        backgroundColor: MaterialStateProperty.all(color),
      ),
      onPressed: onPressed,
      child: loading != null && loading
          ? DotLoader(
              dotOneColor: Colors.white,
              dotTwoColor: Colors.white,
              dotThreeColor: Colors.white,
            )
          : icon != null
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Icon(icon,
                          color: color == GRAY_COLOR
                              ? Colors.black.withOpacity(0.5)
                              : null),
                      flex: 1,
                    ),
                    SizedBox(width: 20),
                    Expanded(
                      flex: 3,
                      child: Text(
                        text,
                        style: TextStyle(
                            fontSize: 16,
                            color: color == GRAY_COLOR
                                ? Colors.black.withOpacity(0.5)
                                : null),
                      ),
                    ),
                  ],
                )
              : Text(
                  text,
                  style: TextStyle(fontSize: 16),
                ),
    );
  }
}
