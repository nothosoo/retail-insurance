import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:silicone/models/user.dart';

Future<User> getLocalUser() async {
  FlutterSecureStorage storage = FlutterSecureStorage();
  String localUser = await storage.read(key: 'user');
  if (localUser != null) {
    var user = User.fromJson(jsonDecode(localUser));
    if (user is User) {
      return user;
    }
  }
  return null;
}

Future setLocalUserAccessToken(String token) async {
  FlutterSecureStorage storage = FlutterSecureStorage();
  String localUser = await storage.read(key: 'user');
  if (localUser != null) {
    var user = User.fromJson(jsonDecode(localUser));
    if (user is User) {
      user.accessToken = token;
      storage.write(key: 'user', value: jsonEncode(user));
    }
  }
  return null;
}

Future removeLocalUser() async {
  FlutterSecureStorage storage = FlutterSecureStorage();
  await storage.delete(key: 'user');
}
