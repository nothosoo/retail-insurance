import 'package:dio/dio.dart';

class Api {
  Dio _dio;

  Api({token = false, firebaseKey = ''}) {
    _dio = Dio();
    if (token != false)
      _dio.options.headers = {'Authorization': 'Bearer ' + token};
    if (firebaseKey != '')
      _dio.options.headers = {'Authorization': firebaseKey};
  }

  Future get(String url) async {
    try {
      Response res = await _dio.get(url);
      return res;
    } on DioError catch (e) {
      if (e.response.statusCode == 401) {
        return null;
      }
    } catch (e) {
      print(e);
    }
  }

  Future post(String url, Map body) async {
    try {
      Response res = await _dio.post(url, data: body);
      return res;
    } catch (e) {
      print(e);
    }
  }
}
