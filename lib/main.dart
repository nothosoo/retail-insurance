import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:silicone/components/loader.dart';
import 'package:silicone/notification_handler.dart';
import 'package:silicone/pages/offline_page.dart';
import 'package:silicone/pages/wrapper.dart';
import 'package:silicone/providers/auth_provider.dart';
import 'package:silicone/providers/notification_provider.dart';
import 'package:silicone/providers/program_provider.dart';
import 'package:silicone/providers/user_provider.dart';
import 'package:silicone/utils/connectivity.dart';
import 'package:silicone/utils/constant.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:rxdart/rxdart.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:connectivity/connectivity.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

/// Streams are created so that app can respond to notification-related events
/// since the plugin is initialised in the `main` function
final BehaviorSubject<ReceivedNotification> didReceiveLocalNotificationSubject =
    BehaviorSubject<ReceivedNotification>();

final BehaviorSubject<String> selectNotificationSubject =
    BehaviorSubject<String>();

const MethodChannel platform = MethodChannel('flutter_local_notifications');

class ReceivedNotification {
  ReceivedNotification({
    @required this.id,
    @required this.title,
    @required this.body,
    @required this.payload,
  });

  final int id;
  final String title;
  final String body;
  final String payload;
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final NotificationAppLaunchDetails notificationAppLaunchDetails =
      await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();

  const AndroidInitializationSettings initializationSettingsAndroid =
      AndroidInitializationSettings('app_icon');

  /// Note: permissions aren't requested here just to demonstrate that can be
  /// done later
  final IOSInitializationSettings initializationSettingsIOS =
      IOSInitializationSettings(
          requestAlertPermission: false,
          requestBadgePermission: false,
          requestSoundPermission: false,
          onDidReceiveLocalNotification:
              (int id, String title, String body, String payload) async {
            didReceiveLocalNotificationSubject.add(ReceivedNotification(
                id: id, title: title, body: body, payload: payload));
          });
  final InitializationSettings initializationSettings = InitializationSettings(
      initializationSettingsAndroid, initializationSettingsIOS);
  await flutterLocalNotificationsPlugin.initialize(initializationSettings,
      onSelectNotification: (String payload) async {
    if (payload != null) {
      debugPrint('notification payload: $payload');
    }
    selectNotificationSubject.add(payload);
  });
  runApp(App(notificationAppLaunchDetails));
}

class App extends StatefulWidget {
  final NotificationAppLaunchDetails notificationAppLaunchDetails;
  bool get didNotificationLaunchApp =>
      notificationAppLaunchDetails?.didNotificationLaunchApp ?? false;
  const App(
    this.notificationAppLaunchDetails, {
    Key key,
  }) : super(key: key);
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  bool _initialized = false;
  bool _error = false;
  final GlobalKey<NavigatorState> navigatorKey =
      new GlobalKey<NavigatorState>();
  Map _source = {ConnectivityResult.none: false};
  NetworkConnectivity _connectivity = NetworkConnectivity.instance;

  void initializeFlutterFire() async {
    try {
      // Wait for Firebase to initialize and set `_initialized` state to true
      await Firebase.initializeApp();
      setState(() {
        _initialized = true;
      });
      _requestPermissions();
      _configureDidReceiveLocalNotificationSubject();
      _configureSelectNotificationSubject();
      await _registerOnFirebase();
      getMessage();
    } catch (e) {
      // Set `_error` state to true if Firebase initialization fails
      setState(() {
        _error = true;
      });
    }
  }

  @override
  void initState() {
    initializeFlutterFire();
    super.initState();
    _connectivity.initialise();
    _connectivity.networkStream.listen((source) {
      setState(() => _source = source);
    });
  }

  _registerOnFirebase() async {
    FirebaseMessaging.instance.subscribeToTopic('all');
    String token = await FirebaseMessaging.instance.getToken();
    FlutterSecureStorage storage = FlutterSecureStorage();
    String fToken = await storage.read(key: STORAGE_KEY_FIREBASE_TOKEN);
    if (fToken == null) {
      await storage.write(key: STORAGE_KEY_FIREBASE_TOKEN, value: token);
    }
    print(token);
  }

  static Future<dynamic> onBackgroundMessage(RemoteMessage message) async {
    await _showBackgroundNotification(message.data);
  }

  static Future<void> _showBackgroundNotification(
      Map<String, dynamic> data) async {
    const AndroidNotificationDetails androidPlatformChannelSpecifics =
        AndroidNotificationDetails(
            'dexterous.com/flutter/local_notifications',
            'dexterous.com/flutter/local_notifications',
            'dexterous.com/flutter/local_notifications',
            importance: Importance.Max,
            priority: Priority.High,
            ticker: 'ticker');
    const iOS = IOSNotificationDetails();
    const NotificationDetails platformChannelSpecifics =
        NotificationDetails(androidPlatformChannelSpecifics, iOS);
    var body = jsonDecode(data['body']);
    await flutterLocalNotificationsPlugin.show(
      int.parse(data['id']),
      body['title'],
      body['content'],
      platformChannelSpecifics,
      payload: data['body'],
    );
  }

  static Future<void> _showNotification(Map<String, dynamic> data) async {
    const AndroidNotificationDetails androidPlatformChannelSpecifics =
        AndroidNotificationDetails(
      'retail-app',
      'retail-main-notification',
      'retail notification channel',
      importance: Importance.Max,
      priority: Priority.High,
      ticker: 'ticker',
    );
    const iOS = IOSNotificationDetails();
    const NotificationDetails platformChannelSpecifics =
        NotificationDetails(androidPlatformChannelSpecifics, iOS);
    var body = jsonDecode(data['body']);
    await flutterLocalNotificationsPlugin.show(
      int.parse(data['id']),
      body['title'],
      body['content'],
      platformChannelSpecifics,
      payload: data['body'],
    );
  }

  void getMessage() {
    FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
      await _showNotification(message.data);
    });
    FirebaseMessaging.onBackgroundMessage(onBackgroundMessage);
  }

  void _requestPermissions() {
    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            IOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
          alert: true,
          badge: true,
          sound: true,
        );
  }

  void _configureDidReceiveLocalNotificationSubject() {
    // didReceiveLocalNotificationSubject.stream
    //     .listen((ReceivedNotification receivedNotification) async {
    //   await showDialog(
    //     context: context,
    //     builder: (BuildContext context) => CupertinoAlertDialog(
    //       title: receivedNotification.title != null
    //           ? Text(receivedNotification.title)
    //           : null,
    //       content: receivedNotification.body != null
    //           ? Text(receivedNotification.body)
    //           : null,
    //       actions: <Widget>[
    //         CupertinoDialogAction(
    //           isDefaultAction: true,
    //           onPressed: () async {
    //             Navigator.of(context, rootNavigator: true).pop();
    //             await Navigator.push(
    //               context,
    //               MaterialPageRoute<void>(
    //                 builder: (BuildContext context) =>
    //                     SecondScreen(receivedNotification.payload),
    //               ),
    //             );
    //           },
    //           child: const Text('Ok'),
    //         )
    //       ],
    //     ),
    //   );
    // });
  }

  void _configureSelectNotificationSubject() {
    selectNotificationSubject.stream.listen((String payload) async {
      await navigatorKey.currentState.push(
        MaterialPageRoute<void>(
            builder: (BuildContext context) =>
                NotificationHandler(payload: payload)),
      );
    });
  }

  @override
  void dispose() {
    didReceiveLocalNotificationSubject.close();
    selectNotificationSubject.close();
    _connectivity.disposeStream();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (_error) {
      return MaterialApp(
        title: 'Test',
        theme: mainTheme,
        home: Text('Error'),
      );
    }

    if (!_initialized) {
      return MaterialApp(
        title: 'Test',
        theme: mainTheme,
        home: Container(
          child: Center(
            child: DotLoader(
              dotOneColor: PRIMARY_COLOR,
              dotTwoColor: PRIMARY_COLOR,
              dotThreeColor: PRIMARY_COLOR,
            ),
          ),
        ),
      );
    }

    if (_source.keys.toList()[0] == ConnectivityResult.none)
      return OfflineScreen();

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => AuthProvider(navigatorKey)),
        ChangeNotifierProvider(create: (_) => ProgramProvider()),
        ChangeNotifierProvider(create: (_) => UserProvider()),
        ChangeNotifierProvider(create: (_) => NotificationProvider()),
      ],
      child: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus &&
              currentFocus.focusedChild != null) {
            currentFocus.focusedChild.unfocus();
          }
        },
        child: MaterialApp(
          title: 'Retail insurance',
          theme: mainTheme,
          home: Wrapper(),
          navigatorKey: navigatorKey,
        ),
      ),
    );
  }
}
